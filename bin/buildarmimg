#! /bin/bash

# Set globals
LIBDIR=/usr/share/manjaro-arm-tools/lib
KEEPROOTFS=true
EXTRACTED=false
FACTORY=false
BMAP=false
CUSTOM_REPO=

#imports
source $LIBDIR/functions.sh
CHROOTDIR=$ROOTFS_IMG/rootfs_$ARCH
PKG_CACHE=$CHROOTDIR/var/cache/pacman/pkg

# check if root
if [ "$EUID" -ne 0 ]
  then echo "This script requires root permissions to run. Please run as root or with sudo!"
  exit
fi

#Arguments 
opt=":e:d:v:i:b:nfxhk:mos:c"

while getopts "${opt}" arg; do
  case $arg in
    c)
      COLORS=false
      ;;
    e)
      EDITION="${OPTARG}"
      ;;
    d)
      DEVICE="${OPTARG}"
      ;;
    v)
      VERSION="${OPTARG}"
      ;;
    n)
      KEEPROOTFS=false
      ;;
    x)
      EXTRACTED=true
      ;;
    i)
      ADD_PACKAGE="${OPTARG}"
      ;;
    b)
      BRANCH="${OPTARG}"
      ;;
    f)
      FACTORY=true
      ;;
    m)
      BMAP=true
     ;;
    k)
      CUSTOM_REPO="${OPTARG}"
     ;;
    s)
      HOSTNAME="${OPTARG}"
     ;;
    \?)
      echo "Invalid option: -${OPTARG}"
      exit 1
      ;;
    h|?)
      usage_build_img
      exit 1
      ;;
    :)
      echo "Option -${OPTARG} requires an argument."
      exit 1
      ;;
  esac
done

[[ "$COLORS" = "true" ]] && enable_colors

IMGNAME=Manjaro-ARM-$EDITION-$DEVICE-$VERSION
ARCH='aarch64'

if [ ! -d "$PROFILES/arm-profiles" ]; then
    getarmprofiles
fi

#Make sure only a known branch is used
if [[ "$BRANCH" != "stable" && "$BRANCH" != "testing" && "$BRANCH" != "unstable" ]]; then
    msg "Unknown branch. Please use either, stable, testing or unstable!"
    exit 1
fi

# start the timer
timer_start=$(get_timer)

#Package lists
PKG_DEVICE=$(grep "^[^#;]" $PROFILES/arm-profiles/devices/$DEVICE | awk '{print $1}')
case "$DEVICE" in
    pinephone)
        PKG_EDITION=$(grep "^[^#;]" $PROFILES/arm-profiles/editions/$EDITION | awk '{sub(/>pinephone/,""); print $1}')
        cat $PROFILES/arm-profiles/services/$EDITION | sed -e '/^#/d' -e 's/>pinephone //g' >$srv_list
        ;;
    *)
        PKG_EDITION=$(grep "^[^#;]" $PROFILES/arm-profiles/editions/$EDITION | awk '{sub(/>pinephone.*/,""); print $1}')
        cat $PROFILES/arm-profiles/services/$EDITION | sed -e '/^#/d' -e '/>pinephone/d' >$srv_list
        ;;
esac

# Creating the rootfs used for the image.
create_rootfs_img
create_img
if [ ${BMAP} = true ]; then
    create_bmap
fi
if [[ "$EXTRACTED" = "true" ]]; then
    info "Image not compressed, because you supplied the -x option!"
else
    # Create the compressed file, from the .img file which will get deleted.
    compress
fi

# show timer
show_elapsed_time "${FUNCNAME}" "${timer_start}"
